import datetime

from aws_cdk import (
    core as cdk,
    aws_codedeploy as codedeploy,
    aws_lambda as lambda_
)


# For consistency with other languages, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.


class CdkExampleStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here

        self.lambda_code = lambda_.Code.from_cfn_parameters()

        func = lambda_.Function(self, "Lambda",
                                code=self.lambda_code,
                                handler="index.main",
                                runtime=lambda_.Runtime.NODEJS_10_X,
                                description="Function generated on {}".format(datetime.datetime.now()),
                                )

        alias = lambda_.Alias(self, "LambdaAlias", alias_name="Prod",
                              version=func.current_version)

        codedeploy.LambdaDeploymentGroup(self, "DeploymentGroup",
                                         alias=alias,
                                         deployment_config=
                                         codedeploy.LambdaDeploymentConfig.LINEAR_10_PERCENT_EVERY_1_MINUTE
                                         )
