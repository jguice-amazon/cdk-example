#!/usr/bin/env python3

# For consistency with TypeScript code, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.
import os

from aws_cdk import core
from cdk_example.cdk_example_stack import CdkExampleStack
from cdk_example.pipeline_stack import PipelineStack

app = core.App()
CdkExampleStack(app, "CdkExampleStack",
                # If you don't specify 'env', this stack will be environment-agnostic.
                # Account/Region-dependent features and context lookups will not work,
                # but a single synthesized template can be deployed anywhere.

                # Uncomment the next line to specialize this stack for the AWS Account
                # and Region that are implied by the current CLI configuration.

                # env=core.Environment(account=os.getenv('CDK_DEFAULT_ACCOUNT'), region=os.getenv('CDK_DEFAULT_REGION')),

                # Uncomment the next line if you know exactly what Account and Region you
                # want to deploy the stack to. */

                # env=core.Environment(account='123456789012', region='us-east-1'),

                # For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html
                )

# CODECOMMIT_REPO_NAME = "pipeline"
BITBUCKET_REPO_NAME = "cdk-example"
BITBUCKET_CONN_ARN = os.getenv('BITBUCKET_CONN_ARN')  # NOTE must be created independently

app = core.App()

lambda_stack = CdkExampleStack(app, "LambdaStack")

PipelineStack(app, "PipelineDeployingLambdaStack",
              lambda_code=lambda_stack.lambda_code,
              repo_name=BITBUCKET_REPO_NAME)

app.synth()
